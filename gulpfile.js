const gulp = require('gulp');
const minify = require('gulp-minify');                 //minify js
const minifyCSS = require('gulp-minify-css');          //minify css
const rename = require('gulp-rename');                 //birleshdirmekchun
const concat = require('gulp-concat');                 //birleshdirmekchun
const nodemon = require('gulp-nodemon');               //server qaldirir
const browserSync = require('browser-sync').create();  //refresh etmeden ishdetmekchun
const reload = browserSync.reload; 
const imagemin = require('gulp-imagemin');             //minify image
var fontmin = require('gulp-fontmin');                 //minify fonts

gulp.task('browser-sync', ['start'], function () {
	browserSync.init({
		server: {
			baseDir: './'
		}
	})
	gulp.watch('./*.html').on('change', reload)
	gulp.watch('source/images/**/*', ['images']);
	gulp.watch('source/libs/OwlCarousel/**/*.css', ['owlStyles']);
	gulp.watch('source/libs/OwlCarousel/**/*.js', ['owlJs']);
	gulp.watch('source/js/**/*.js', ['scripts']);
	gulp.watch('source/css/**/*.css', ['styles']);
})

gulp.task('start', function() {
	return nodemon({
		script: 'server.js'
	})
})

gulp.task('html', (done) => {
	browserSync.reload()
	done()
})

gulp.task('fonts', () => {
	gulp.src('source/fonts/*')
        .pipe(fontmin({
            text: '天地玄黄 宇宙洪荒',
        }))
        .pipe(gulp.dest('dist/fonts'));
})


gulp.task('styles', () => {
	gulp.src(['source/css/**/*.css'])
	.pipe(minifyCSS())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dist/css'))
	.pipe(browserSync.stream())
})

gulp.task('images', () => {
	gulp.src(['source/images/**/*'])
	.pipe(imagemin())
    .pipe(gulp.dest('dist/images'))
	.pipe(browserSync.stream())
})

gulp.task('scripts', () => {
	gulp.src(['source/js/**/*.js'])
	.pipe(concat('all.js'))
	.pipe(minify({
		ext: {
			min: '.min.js'
		},
		noSource: true
	}))
	.pipe(gulp.dest('dist/js'))
	.pipe(browserSync.stream())
});

gulp.task('owlStyles', () => {
	gulp.src(['source/libs/OwlCarousel/css/**/*.css'])
	.pipe(minifyCSS())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dist/OwlCarousel/css'))
	.pipe(browserSync.stream())
})


gulp.task('owlJs', () => {
	gulp.src(['source/libs/OwlCarousel/js/**/*.js'])
	.pipe(concat('all.js'))
	.pipe(minify({
		ext: {
			min: '.min.js'
		},
		noSource: true
	}))
	.pipe(gulp.dest('dist/OwlCarousel/js'))
	.pipe(browserSync.stream())
});


gulp.task('default', ['browser-sync', 'html', 'images', 'fonts', 'owlStyles', 'owlJs', 'styles', 'scripts']);