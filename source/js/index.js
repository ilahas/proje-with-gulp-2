$(document).ready(function(){
    // testimonials slider
     var owl = $('.owl-carousel');
      owl.owlCarousel({
        items: 3,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1
            },
            1000:{
                items:3
            }
        }
      });

});